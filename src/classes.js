/*
* Copyright (C) 2014 Bryan Learn
* License: http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
*
* File: classes.js
* Purpose: All game classes for LD30 - Connected Worlds.
*/

/* Small Enemy */
var smBad = function (x,y,color) {

  // Sprite info
  var x = x;
  var y = y;
  var size = 16;
  var color = color;
  var velX = 0;
  var velY = 0;
  
  // Stats
  var health = 100;
  var ammo = 100;
  var speed = 5;

  this.update = function () {
    velX = speed * (2*Math.random()-1);
    velY = speed * (2*Math.random()-1);

    x += velX;
    y += velY;
  };

  this.draw = function (ctx) {
    ctx.fillStyle = color;
    ctx.sFillRect(x,y,size,size);
  };

};

/* Medium Enemy */
var mdBad = function (x,y,color) {
  var x = x;
  var y = y;
  var size = 32;
  var color = color;
  
  // Stats
  var health = 100;
  var ammo = 100;
  var speed = 5;

  this.update = function () {
    
  };

  this.draw = function () {
    ctx.fillStyle = color;
    ctx.sFillRect(x,y,size,size);
  };
};

/* Boss */
var lgBad = function (x,y,color) {
  var x = x;
  var y = y;
  var size = 128;
  var color = color;

  // Stats
  var health = 100;
  var ammo = 100;
  var speed = 5;

  this.update = function () {

  };

  this.draw = function () {
    ctx.fillStyle = color;
    ctx.sFillRect(x,y,size,size);
  };
};

/* Main Character */
var mChar = function (x,y,color) {
  // Sprite info
  var loc = new PVector(x,y);
  var vel = new PVector(0,0);
  var size = 32;
  var color = color;
  
  // Stats
  var health = 100;
  var ammo = 1000;
  var speed = 25;
  var bulletSpeed = 20;
  var burstCnt = 3;

  // Inventory
  var hasRed = false;
  var hasYellow = false;
  var hasBlue = false;

  // Weapons & Shields
  var bulletBuff = new CircularBuff(ammo);
  for(var i=0; i<bulletBuff.size(); i++){ // fill buffer with bullets
    bulletBuff.add(new Bullet());
  }
  var shield = null;
  
  this.update = function (ctrl) {
    console.log(ammo);

    // update character
    if(ctrl.isKeyDown(ctrl.up)) { loc.add( new PVector(0, speed*-1) ); }
    if(ctrl.isKeyDown(ctrl.down)) { loc.add( new PVector(0, speed) ); }
    if(ctrl.isKeyDown(ctrl.left)) { loc.add( new PVector(speed*-1, 0) ); }
    if(ctrl.isKeyDown(ctrl.right)) { loc.add( new PVector(speed, 0) ); }

    if(mouse.btn1Down()) { fire( new PVector(mouse.x(), mouse.y()) ); }
    
    // update bullets
    for(var i=0; i<bulletBuff.size(); i++){
      if( bulletBuff.getElem(i).isActive() ){
        bulletBuff.getElem(i).update();

        if(bulletBuff.getElem(i).getLifetime() <= 0){
          ammo++;
        }
      }
    }
  };

  this.draw = function (ctx) {
    // draw bullets
    for(var i=0; i<bulletBuff.size(); i++){
      if( bulletBuff.getElem(i).isActive() ){
        //process bullet by type
        switch( bulletBuff.getElem(i).getType() ){
          case 0: // normal bulletBuff
            ctx.beginPath();
            ctx.sArc(bulletBuff.getElem(i).getX(), bulletBuff.getElem(i).getY(), bulletBuff.getElem(i).getSize(), 0, 2*Math.PI, false);
            ctx.fillStyle = 'rgba(200,200,200,.6)';
            ctx.fill();
            break;
          case 1: // red powerup
            break;
          case 2: // orange powerup
            break;
          case 3: // blue powerup
            break;
          case 4: // green powerup 
            break;
        }
      }
    }

    // draw shooter
    ctx.fillStyle = color;
    ctx.sFillRect(loc.getX()-(size/2), loc.getY()-(size/2), size, size);

  };

  var fire = function(mLocVec) {
    for(var i=0; i<burstCnt; i++){
      if( !bulletBuff.nextElem().isActive() ){
        // calculate velocity vector
        var velVec = new PVector(0,0);
        velVec.add(mLocVec);
        velVec.randErr(0.1);
        velVec.sub(loc);
        velVec.normalize();
        velVec.mult(bulletSpeed);

        var locVec = new PVector(0,0);
        locVec.add(loc);
        locVec.randErr(0.1);

        // create bullet
        bulletBuff.nextElem().reuse(locVec,velVec,10,100,0);
        bulletBuff.step();
        ammo--;
      }
    } // end burst
  };

  this.setBS = function(val) { bulletSpeed = val; };
  this.setBC = function(val) { burstCnt = val; };
};

/* Bullet 
 * Modified to use as bullet, bomb, or shield
 */
var Bullet = function() {
  var loc = new PVector(0,0);
  var vel = new PVector(0,0);
  var size = 0;
  var type = null;
  var tty = 0;
  var active = false;

  this.reuse = function(lv,vv,s,l,t) {
    loc.copy(lv);
    vel.copy(vv);
    size = s;
    tty = l;
    type = t;
    active = true;
    this.onCreate();
  }.bind(this);

  this.destroy = function() {
    active = false;
    this.onKill();
  }.bind(this);

  this.getX = function() { return loc.getX(); };
  this.getY = function() { return loc.getY(); };
  this.getLoc = function() { return loc; };
  this.getSize = function() { return size; };
  this.getLifetime = function() { return tty; };
  this.getType = function() { return type; };
  this.isActive = function() { return active; };
  this.getVel = function() { return vel; };

  this.setLoc = function(val) { loc.copy(val); };
  this.setSize = function(val) { size = val; };
  this.setLifetime = function(val) { tty = val; };
  this.tick = function() { tty--; };
  this.setType = function(val) { type = val; };
  this.setVel = function(val) { vel.copy(val); };

  this.update = function() {
    loc.add(vel);
    tty--;
    if(tty <= 0){
        active = false;
        this.onKill();
    }
  }.bind(this);

  this.onKill = function(){};
  this.onCreate = function(){};

};

/* Planet */
var Planet = function (x,y, pSrc, bSrc) {
  // Sprite info
  var x = x;
  var y = y;
  var pImg = new Image();
  pImg.src = pSrc;
  var bImg = new Image();
  bImg.src = bSrc;

  // Stats
  var hasBase = true;

  this.update = function () {

  };

  this.draw = function (ctx) {
    // draw planet
    ctx.sDrawImage(pImg, x, y, pImg.width, pImg.height);

    //if boss still alive
    if(hasBase){
      // draw base
      ctx.sDrawImage(bImg, x, y-127, bImg.width, bImg.height);
    }
  };

  this.setBase = function(bool) { hasBase = bool; };
};
