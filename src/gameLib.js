/*
* Copyright (C) 2014 Bryan Learn
* License: http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
*
* File: gameLib.js
* Purpose: Holds all of the classes used for the game framework.
*/

var requestAnimFrame = window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame    ||
  window.oRequestAnimationFrame      ||
  window.msRequestAnimationFrame     ||
  function(callback){ window.setTimeout(callback, 100) };

// GameLoop
var GameLoop = function (i,d,u,e){

  var running = false;
  var isPause = true;

  var nextTime = (new Date).getTime(); // time in milliseconds
  var currTime = null;
  var delta = 1000;
  var maxTimeDiff = delta * 3; // how "in sync" update & draw are
  var skippedFrames = 1; // counter for # of consecutive skipped frames
  var maxSkippedFrames = 5; // how many frames can be skipped var scaleY = window.innerHeight/resY;

  /* abstract functions */
  this.init = i;
  this.draw = d; 
  this.update = u;
  this.end = e;

  /* public functions */

  this.start = function () {
    this.init();
    run();
  }.bind(this);

  this.onpause = null;
  this.offpause = null;

  this.stop = function () {
    this.end();
    shutdown();
  }.bind(this);

  var loop = function() {
    if( !isPause ) {
        currTime = (new Date).getTime(); // time in milliseconds

        //console.log('[Run Loop] cur: '+currTime+' next: '+nextTime);

        if( (currTime - nextTime) > maxTimeDiff ){ nextTime = currTime } // recently rendered a frame?
        if( currTime >= nextTime ) { // is it time to update?
          // set next update time
          nextTime += delta;

          // update logic
          this.update();
          
          // render
          if( (currTime < nextTime) || (skippedFrames > maxSkippedFrames) ) { // Do we have enough time? Have we skipped too many frames?
              this.draw();
              skippedFrames = 1; // reset the counter
          }
          else {
            skippedFrames++; // we skipped a frame
          }
        }
        else {
          // sleep for .. ?
          var sleepTime = nextTime - currTime;

          if( sleepTime > 0 ){
            // sleep
            wait(sleepTime);
          }

        }
      } // end if(!pause)

      if(running){ requestAnimFrame(loop); }
      else { shutdown(); }
  }.bind(this);

  // param delta: time step in ms (17: ~60fps; 34: ~30fps; 67: ~15fps)
  var run = function () {
    running = true;
    requestAnimFrame(loop);
  }.bind(this); // end of Run()


  var shutdown = function () {
    running = false; 
  }.bind(this);

  /* getters */
  this.isRunning = function () { return running; };
  this.isPaused = function () { return isPause; };

  /* setters */
  this.setDelta = function (dt) { delta = dt; };

  // Busy loop that runs for ms milliseconds
  var wait = function (ms) {
    var date = new Date();
    date.setTime(date.getTime() + ms);
    while( new Date().getTime() < date.getTime()); // do nothing
    return true;
  };

  this.unpause = function () {
    isPause = false;
    if(this.offpause != null){ this.offpause(); }
  }.bind(this);

  this.pause = function () {
    isPause = true;
    if(this.onpause != null){ this.onpause(); }
  }.bind(this);

}; // end GameLoop class

/* Controller */
var Controller = function () {
  var isDown = new Object();

  /* public vars */
  // default WASD setup
  this.up = 87; // w
  this.down = 83; // s
  this.left = 65; // a
  this.right = 68; // d
  this.btn1 = 32; // Space
  this.btn2 = null;
  this.btn3 = null;
  this.btn4 = null;
  this.btn5 = null;
  this.btn6 = null;
  this.btn7 = null;
  this.btn8 = null;
  this.start = 13; // Enter

  // returns true if key is down
  this.isKeyDown = function(code) {
    if(isDown[code] != undefined){
      return isDown[code];
    }
    else { return false; }
  };

  // set key up/down flag
  this.setKey = function(code, bool) {
    isDown[code] = bool;
  };
}; // end Controller classes

/* Mouse */
var Mouse = function () {
  /* private vars */
  var btn1 = false;
  var btn2 = false;
  var mX = 0;
  var mY = 0;

  /* public vars */
  this.btn1Down = function() {
    return btn1;
  };
  this.btn2Down = function() {
    return btn2;
  };
  this.x = function() {
    return mX;
  };
  this.y = function() {
    return mY;
  };

  this.setBtn1 = function(bool) { btn1 = bool; };
  this.setBtn2 = function(bool) { btn2 = bool; };
  this.setLoc = function(x,y) {
    mX = x;
    mY = y;
  };

};

/* Circular Buffer */
var CircularBuff = function(size) {
  var max = size;
  var buff = new Array(max);
  var next = 0;

  this.add = function(element) {
    buff[next] = element;
    this.step();
  }.bind(this);

  this.step = function() {
    next++;
    if(next >= max){
      next = 0;
    }
  };

  this.nextElem = function() { return buff[next]; };
  this.nextIndx = function() { return next; };
  this.getElem = function(val) { return buff[val]; };
  this.size = function() { return max; };
};
