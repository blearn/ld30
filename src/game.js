/*
* Copyright (C) 2014 Bryan Learn
* License: http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
*
* File: game.js
* Purpose: Handle's user input and game data.
*/

var engine;
var canvas = null;
var ctx = null;

// Controller and Mouse
var ctrl = null;
var mouse = null;

var resX = 1920; // hd, baby
var resY = 1080;
var scaleX = window.innerWidth/resX;
var scaleY = window.innerWidth/resY;

function onload() {
  console.log('loading');
  engine = new GameLoop(myInit, myDraw, myUpdate);
  engine.onpause = myPause;
  engine.setDelta(34); //67: ~15 updates/sec; 34: ~30 updates/sec

  engine.unpause();
  engine.start();
}

function myDraw() {
  //clear screen
  ctx.clearRect(0,0,canvas.width,canvas.height);

  // draw background
  ctx.fillStyle = 'rgba(0,0,0,1)';
  ctx.sFillRect(0,0,resX,resY);

  // draw entities
 
  for(var i=0; i<planets.length; i++){
    planets[i].draw(ctx);
  }

  for(var i=0; i<baddies.length; i++){
    baddies[i].draw(ctx);
  }

  shooter.draw(ctx);

  // draw UI

}

function myPause() {
  ctx.fillStyle = "rgba(100,100,100,.4)";
  ctx.fillRect(0,0,canvas.width, canvas.height);
}

function myUpdate() {
  // collision detection (fuck, try simple radius detection)
 
  // update entity positions
  for(var i=0; i<baddies.length; i++){
    baddies[i].update(ctrl);
  }

  shooter.update(ctrl); 
}

function myInit() {
  // get canvas & context
  canvas = document.getElementById("canvas");
  if( canvas.getContext) {
    ctx = canvas.getContext("2d");
    ctx.fillOval = function(x,y,w,h) {
     ctx.fillRect(x,y,w,h); 
    };
    ctx.sFillRect = function(x,y,w,h){
      ctx.fillRect(x*scaleX,y*scaleY,w*scaleX,h*scaleY);
    };
    ctx.sDrawImage = function(img,x,y,w,h){
      ctx.drawImage(img,x*scaleX,y*scaleY,w*scaleX,h*scaleY);
    };
    ctx.sArc = function(x,y,r,s,e,c) {
      ctx.arc(x*scaleX,y*scaleY, r*((scaleX+scaleY)/2) , s,e,c);
    };
  }

  // create resize event
  function resizeCanvas () {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    scaleX = canvas.width/resX;
    scaleY = canvas.height/resY;
    //console.log(canvas.width+', '+canvas.height);
  }
  window.addEventListener('resize', resizeCanvas, false);
  resizeCanvas();

  // create input listeners
  canvas.tabIndex = 1000; // allow canvas to have event listeners (blkmagic)
  canvas.onkeydown = keyDown;
  canvas.onkeyup = keyUp;
  canvas.onmousedown = mouseDown;
  canvas.onmouseup = mouseUp;
  canvas.onmousemove = mouseMove;

  ctrl = new Controller();
  mouse = new Mouse();

  // load bg img

  // load ui

  // create char & baddies
  shooter = new mChar(resX/2, resY/2, 'rgba(127,127,0,1)');
  baddies = new Array();
  for(var i=0; i<10; i++){
    baddies[i] = new smBad(resX*Math.random(), resY*Math.random(), 'rgba(255,0,0,.8)');
  }

  // create planets
  planets = new Array();
  redPlanet = new Planet(0,200,'assets/red-planet.png','assets/red-base.png');
  planets.push(redPlanet);

  console.log("game start!");
} // end Init()

/* keyDown */
var keyDown = function(event) {
  ctrl.setKey(event.keyCode, true);

}

/* keyUp */
var keyUp = function(event) {
  ctrl.setKey(event.keyCode, false);
  // check for pause 
  if(event.keyCode == ctrl.start){
    if( !engine.isPaused() ) { engine.pause(); }
    else { engine.unpause(); }
  }
}

/* mouseUp */
var mouseUp = function(event) {
  if(event.button == 0){ mouse.setBtn1(false); }
  if(event.button == 2){ mouse.setBtn2(false); }
};

/* mouseDown */
var mouseDown = function(event) {
  if(event.button == 0){ mouse.setBtn1('true'); }
  if(event.button == 2){ mouse.setBtn2('true'); }
};

/* mouseMove */
var mouseMove = function(event) {
  mouse.setLoc(event.clientX, event.clientY);
};
